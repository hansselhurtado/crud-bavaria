<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('SKU')->comment('referencia de producto');
            $table->string('name')->comment('nombre de producto');
            $table->string('description')->nullable()->comment('descripcion de producto');
            $table->float('price')->comment('precio de producto');
            $table->float('tax')->comment('IVA de producto');
            $table->string('photo')->nullable()->comment('imagen de producto');

            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
