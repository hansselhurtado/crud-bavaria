<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaleTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->comment('nombre del cliente');
            $table->string('email')->unique()->comment('email de cliente');
            $table->bigInteger('phone')->unique()->comment('telefon de cliente');

            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('sales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('invoice')->unique()->comment('numero de factura');
            $table->float('subtotal')->nullable();
            $table->float('tax')->nullable()->comment('IVA de la compa');
            $table->float('total')->nullable()->comment('total de la compa');

            $table->unsignedBigInteger('client_id');
            $table->foreign('client_id')->references('id')->on('clients');

            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('sale_details', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('product_id');
            $table->foreign('product_id')->references('id')->on('products');

            $table->bigInteger('amount')->comment('cantidad de producto');
            $table->float('total')->comment('valor total del detalle de compa');

            $table->unsignedBigInteger('invoice_id');
            $table->foreign('invoice_id')->references('id')->on('sales');

            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_tables');
    }
}
