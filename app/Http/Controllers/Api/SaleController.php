<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\SaleRequest;
use App\Http\Resources\Sale as ResourcesSale;
use App\Http\Resources\SaleCollection;
use App\Models\Product;
use App\Models\Sale;
use App\Models\SaleDetail;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SaleController extends ApiController
{
    protected $sale;
    protected $saleDetail;

    public function __construct(Sale $sale, SaleDetail $saleDetail)
    {
        $this->sale         = $sale;
        $this->saleDetail   = $saleDetail;
    }

    public function index()
    {
        return $this->successResponse(new SaleCollection($this->sale::active()->get()));
    }

    public function show(Sale $invoice)
    {
        try {
            return $this->successResponse(new ResourcesSale($invoice));
        }catch (ModelNotFoundException $e) {
            return $this->errorResponse('Invoice no encontrado', 404);
        }
    }

    public function sale(SaleRequest $request)
    {
        $data       = $request->all();
        $products   = $request->products;
        try {
            DB::beginTransaction();

            $invoice = $this->saveInvoice($data['client_id']);//se guarda la orden de compra
            $this->saveProducts($products, $invoice->id);//se guarda el detalle de la orden

            $invoice->update([
                'subtotal'  => $this->totalSale($invoice),
                'total'     => $this->totalSale($invoice) + ($this->totalSale($invoice) * ($invoice->tax/100))
            ]);//se actualiza el detalle de la compra con los totales

            DB::commit();
            return $this->successResponse(new ResourcesSale($invoice), 'Compra realizada con éxito');

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    private function saveProducts($products, $invoice_id){
        foreach ($products as $value) {
            $product = Product::handleStatus($value['id'])->first();
            if ($product) {
                $this->saleDetail::create([
                    'product_id'    => $value['id'],
                    'amount'        => $value['amount'],
                    'total'         => $product->price_total * $value['amount'],
                    'invoice_id'    => $invoice_id,
                ]);
            }
        }
    }

    private function saveInvoice($client_id){
        return $this->sale::create([
            'invoice'   => rand(10000, 10000000),
            'client_id' => $client_id,
            'tax'       => Sale::TAX,
        ]);
    }

    private function totalSale($invoice){
        $total = 0;
        foreach ($invoice->saleDetails as $saleDetail) {
            $total = $total + $saleDetail->total;
        }
        return $total;
    }
}
