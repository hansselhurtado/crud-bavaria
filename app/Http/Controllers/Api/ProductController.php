<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\Product as ResourcesProduct;
use App\Http\Resources\ProductCollection;
use App\Models\Product;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends ApiController
{
    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function index()
    {
        return $this->successResponse(new ProductCollection($this->product::active()->get()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $data = $request->all();
        try {
            DB::beginTransaction();

            $product        = $this->product::create($data);
            $file           = $request->file('photo');
            $product->photo = $this->saveFile('\product', $file);
            $product->save();

            DB::commit();
            return $this->showMessage('Producto creado exitosamente');

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    function saveFile($path, $file){
        $name = time() . '.' .$file->getClientOriginalName();
        $path = public_path() .$path;
        $file->move($path, $name);
        return $name;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        try {
            return $this->successResponse(new ResourcesProduct($product));
        }catch (ModelNotFoundException $e) {
            return $this->errorResponse('Producto no encontrado', 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        $data = $request->all();
        try {
            DB::beginTransaction();

            $product->update($data);
            if($request->hasFile('photo')){
                $file           = $request->file('photo');
                $product->photo = $this->saveFile('\product', $file);
                $product->save();
            }
            DB::commit();
            return $this->successResponse(new ResourcesProduct($product), 'Producto actualizado');

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        try {
            $product->delete();
            return $this->showMessage('Producto eliminado');
        }catch (ModelNotFoundException $e) {
            return $this->errorResponse('Producto no encontrado', 404);
        }
    }
}
