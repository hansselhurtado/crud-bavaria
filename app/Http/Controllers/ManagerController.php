<?php

namespace App\Http\Controllers;

use App\Http\Requests\ManagerRequest;
use App\Http\Resources\Manager as ResourcesManager;
use App\Http\Resources\ManagerCollection;
use App\Models\Manager;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class ManagerController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            return $this->successResponse(new ManagerCollection(Manager::active()->get()));
        } catch (\Throwable $e) {
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ManagerRequest $request)
    {
        $data = $request->all();
        try {
           Manager::create($data);
           return $this->showMessage('Manager creado correctamente');
        } catch (\Throwable $e) {
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Manager $manager)
    {
        try {
            return $this->successResponse(new ResourcesManager($manager), 'Usuario encontrado');
        } catch (\Throwable $e) {
            return $this->errorResponse('No encontrado', 409);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ManagerRequest $request, Manager $manager)
    {
        
        $data = $request->all();
        try {
           $manager->update($data);
           return $this->showMessage('Manager actualizado correctamente');
        } catch (\Throwable $e) {
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
