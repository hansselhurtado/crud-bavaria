<?php

namespace App\Http\Requests;

use App\Traits\ApiResponser;
use Illuminate\Contracts\Validation\Validator as ValidationValidator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class ManagerRequest extends FormRequest
{
    use ApiResponser;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'email'     => ['bail','required','string', Rule::unique('managers')->whereNull('deleted_at')],
                    'phone'     => ['bail','required','numeric', 'digits_between:7,12', Rule::unique('managers')->whereNull('deleted_at')],
                    'client_id' => 'bail|required|numeric|exists:managers'
                ];
              break;
            case 'PATCH':
            case 'PUT':
                return [
                    'email'     => ['bail','required','string', Rule::unique('managers')->ignore(request('manager')->id)->whereNull('deleted_at')],
                    'phone'     => ['bail','required','numeric', 'digits_between:7,12', Rule::unique('managers')->ignore(request('manager')->id)->whereNull('deleted_at')],
                    'client_id' => 'bail|required|numeric|exists:managers'
                ];
              break;
            default:
            return [
                'email'     => ['bail','required','string', Rule::unique('managers')->whereNull('deleted_at')],
                'phone'     => ['bail','required','numeric', 'digits_between:7,12', Rule::unique('managers')->whereNull('deleted_at')],
                'client_id' => 'bail|required|numeric|exists:managers'
            ];
              break;
        }

    }

    public function failedValidation(ValidationValidator $validator) {
        $message = $validator->errors()->first();
        throw new HttpResponseException($this->showMessage($message, 500, false));
    }
}
