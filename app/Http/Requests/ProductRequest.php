<?php

namespace App\Http\Requests;

use App\Traits\ApiResponser;
use Illuminate\Contracts\Validation\Validator as ValidationValidator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest
{
    use ApiResponser;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'SKU'               => ['bail','required','string', Rule::unique('products')->whereNull('deleted_at')],
                    'name'              => 'bail|required|string|min:2|max:255',
                    'description'       => 'bail|nullable|string|max:555',
                    'price'             => 'bail|required|numeric|min:1',
                    'tax'               => 'bail|required|numeric|digits_between:1,2',
                    'photo'             => 'bail|required|mimes:jpg,jpeg,png'
                ];
              break;
            case 'PATCH':
            case 'PUT':
                return [
                    'SKU'               => ['bail','nullable','string',Rule::unique('products')->ignore(request('product')->id)->whereNull('deleted_at')],
                    'name'              => 'bail|nullable|string|min:2|max:255',
                    'description'       => 'bail|nullable|string|max:555',
                    'price'             => 'bail|nullable|numeric',
                    'tax'               => 'bail|nullable|digits_between:1,2|numeric'
                ];
              break;
            default:
                return [
                    'SKU'               => ['bail','required','string', Rule::unique('products')->whereNull('deleted_at')],
                    'name'              => 'bail|string|min:2|max:255',
                    'description'       => 'bail|string|max:555',
                    'price'             => 'bail|numeric',
                    'tax'               => 'bail|numeric|digits_between:1,50'
                ];
              break;
          }
    }

    public function failedValidation(ValidationValidator $validator) {
        $message = $validator->errors()->first();
        throw new HttpResponseException($this->showMessage($message, 500, false));
    }
}
