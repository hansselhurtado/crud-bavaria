<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SaleDetail extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                                => $this->id,
            'invoice'                           => $this->invoice->invoice,
            'invoice_id'                        => $this->invoice_id,
            'amount'                            => $this->amount,
            'total'                             => $this->total,
            'product'                           => new Product($this->product),
            'status'                            => $this->status,
            'create_at'                         => date_format($this->created_at, 'Y-m-d, h:m'),
            'update_at'                         => date_format($this->updated_at, 'Y-m-d, h:m')
        ];
    }
}
