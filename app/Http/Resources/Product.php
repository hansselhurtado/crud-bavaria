<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                                => $this->id,
            'SKU'                               => $this->SKU,
            'name'                              => $this->name,
            'description'                       => $this->description,
            'price'                             => $this->price,
            'tax'                               => $this->tax,
            'photo'                             => $this->photo,
            'status'                            => $this->status,
            'create_at'                         => date_format($this->created_at, 'Y-m-d, h:m'),
            'update_at'                         => date_format($this->updated_at, 'Y-m-d, h:m')
        ];
    }
}
