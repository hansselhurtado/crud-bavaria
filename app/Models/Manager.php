<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Manager extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'managers';

    protected $fillable = [
        'name',
        'email',
        'phone',
        'client_id',
        'status',
    ];


    public function client() {
        return $this->belongsTo(Client::class);
    }

    // SCOPE
    function scopeActive($query){
        return $query->where('status', 'ACTIVO');
    }

    function scopeHandleClient($query, $num){
        return $query->where('client_id', '>', $num);
    }

}
