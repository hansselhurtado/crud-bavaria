<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaleDetail extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'sale_details';

    protected $fillable = [
        'product_id',
        'amount',
        'total',
        'invoice_id',
        'status',
    ];

    public function product() {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function invoice() {
        return $this->belongsTo(Sale::class, 'invoice_id');
    }
}
