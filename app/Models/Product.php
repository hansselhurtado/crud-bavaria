<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'products';

    protected $fillable = [
        'SKU',
        'name',
        'description',
        'price',
        'tax',
        'photo',
        'status',
    ];

    const ACTIVE = 'ACTIVO';

    public function saleDetails() {
        return $this->hasMany(SaleDetail::class);
    }

    // scope
    public function scopeActive($query){
        return $query->where('status', Product::ACTIVE);
    }

    public function scopeHandleStatus($query, $id){
        return $query->where('id', $id)
                    ->where('status', Product::ACTIVE);
    }

    // ACCESSOR
    function getPriceTotalAttribute(){
        $subTotal = $this->price * ($this->tax/100);
        return $subTotal + $this->price;
    }
}
