<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'sales';

    protected $fillable = [
        'invoice',
        'subtotal',
        'tax',
        'total',
        'client_id',
        'status',
    ];

    const TAX       = 19;
    const ACTIVE    = 'ACTIVO';


    public function saleDetails() {
        return $this->hasMany(SaleDetail::class, 'invoice_id');
    }

    public function client() {
        return $this->belongsTo(Client::class, 'client_id');
    }

    // scope
    public function scopeActive($query){
        return $query->where('status', Sale::ACTIVE);
    }
}
