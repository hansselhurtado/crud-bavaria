<?php

use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\SaleController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\ManagerController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// rutas de recursos
Route::apiResources([
    'product'   => ProductController::class,
    'manager'    => ManagerController::class,
]);


Route::post('sale',                       [SaleController::class, 'sale']);
Route::get('invoice/index',               [SaleController::class, 'index']);
Route::get('invoice/show/{invoice}',      [SaleController::class, 'show'])->missing(function (Request $request) {
    return response()->json([
        'message'       => 'Invoice no encontrado',
        'response'      => false,
        'code'          => 200,
    ]);
});
